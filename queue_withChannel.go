package main

import (
	"fmt"
)

type Queue struct {
	Items []int
}

func (q *Queue) Push(item int) {
	q.Items = append(q.Items, item)
}

func (q *Queue) Pop() int {
	if len(q.Items) == 0 {
		return 0
	}
	item := q.Items[0]
	q.Items = q.Items[1:]
	return item
}

func main() {
	q := new(Queue)
	ch := make(chan int, 1000) // Buffered channel with 1000 channels
	// Sending data into the channel
	for i := 0; i < 100; i++ {
		ch <- i
	}
	/*Close the channel so that nothing can be writing in it.
	If you forget it then the code will get into deadlock as the channel
	still is waiting for someone to write to the channel, so the channel remains blocked.
	Closing the channel doesn't stop use from reading from it, but stops us from writing into it.
	We will read the channel and push it into our queue.
	*/
	close(ch)
	// pushing the data into the queue from the channel
	for v := range ch {
		q.Push(v)
		fmt.Printf("len=%d\n", len(q.Items))
	}
	// Popping the data from the queue with FIFO rule.
	for len(q.Items) > 0 {
		i := q.Pop()
		fmt.Printf("len=%d\n", len(q.Items))
		fmt.Printf("pop=%d\n", i)
	}
}
