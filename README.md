A simple Stack and Queue data structure using Golang.

Usage:
```
 go run stack.go
 go run queue.go
 go run queue_withChannel.go

```

I have used Mutex (sync package) to make the Stack and Queue thread safe. You can also use go channels to achieve the same. If you know the size of your data then using channel will provide you better performance as with Mutex the program will spend a lot of time waiting on locks instead of execution when the data gets large.  
