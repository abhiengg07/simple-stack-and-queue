package main

import (
	"fmt"
	"sync"
)

type stack struct {
	data []int
	sync.Mutex
}

func (s stack) Empty() bool { return len(s.data) == 0 }
func (s stack) Peek() int   { return s.data[len(s.data)-1] }
func (s stack) Len() int    { return len(s.data) }
func (s *stack) Put(i int) {
	s.Lock()
	defer s.Unlock()
	s.data = append(s.data, i)
}
func (s *stack) Pop() int {
	s.Lock()
	defer s.Unlock()
	d := s.data[len(s.data)-1]
	s.data = s.data[:len(s.data)-1]
	return d
}

func main() {
	s := new(stack)
	for i := 0; i < 3; i++ {
		s.Put(i)
		fmt.Printf("len=%d\n", len(s.data))
		fmt.Printf("peek=%d\n", s.Peek())
	}

	for !s.Empty() {
		i := s.Pop()
		fmt.Printf("len=%d\n", len(s.data))
		fmt.Printf("pop=%d\n", i)
	}
}
