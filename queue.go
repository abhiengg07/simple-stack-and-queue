package main

import (
	"fmt"
	"sync"
)

type Queue struct {
	sync.Mutex
	Items []int
}

func (q *Queue) Push(item int) {
	q.Lock()
	defer q.Unlock()
	q.Items = append(q.Items, item)
}

func (q *Queue) Pop() int{
	q.Lock()
	defer q.Unlock()
	if len(q.Items) == 0 {
		return 0
	}
	item := q.Items[0]
	q.Items = q.Items[1:]
	return item
}

func main() {
  q := new(Queue)
  for i := 0; i < 100; i++ {
		q.Push(i)
		fmt.Printf("len=%d\n", len(q.Items))
	}

	for len(q.Items) > 0 {
		i := q.Pop()
		fmt.Printf("len=%d\n", len(q.Items))
		fmt.Printf("pop=%d\n", i)
	}
}
